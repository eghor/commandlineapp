﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Microlite.Extensions.MsCommandLineUtils;

namespace ExampleApp.Commands.OpenBrowser
{
    public class OpenBrowserCommand : Command<OpenBrowserParameters>
    {
        public override string Name => "openbrow";

        public override string Desc => "opens a browser window from cli console";

        public override int Run(OpenBrowserParameters settings)
        {
            var url = settings.Url;

            try
            {
                if (string.IsNullOrEmpty(url)) 
                {
                    Console.WriteLine("no url argument supplied.\nOpening google.co.uk instead...");
                    url = "https://www.google.co.uk/";
                }

                Process.Start(url);

                return 0;
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }

                return 1;
            }
        }
    }
}
