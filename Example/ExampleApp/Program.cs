﻿using ExampleApp.Commands.OpenBrowser;
using Microlite.Extensions.MsCommandLineUtils;

namespace ExampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new CommandApp();

            app.Register<OpenBrowserCommand, OpenBrowserParameters>();

            app.Execute(args);
        }
    }
}