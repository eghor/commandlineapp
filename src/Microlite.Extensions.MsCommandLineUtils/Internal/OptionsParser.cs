﻿using Microlite.Extensions.MsCommandLineUtils.Attributes;
using Microlite.Extensions.MsCommandLineUtils.Core.Adapter;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Internal
{
    internal class OptionsParser : Parser<OptMap,OptAttribute>
    {

        public override OptMap ConfigureMapType(OptAttribute attribute)
        {
            return new OptMap
            {
                MsOption = new CommandOption(attribute.Template, attribute.From)
                {
                    Description = attribute.Desc
                }
            };
        }
    }
}
