﻿using System.Collections.Generic;
using System.Linq;
using Microlite.Extensions.MsCommandLineUtils.Core;
using Microlite.Extensions.MsCommandLineUtils.Core.Adapter;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Internal
{
    internal class CommandSetup : ISetup
    {
        #region init
        private readonly ISettingsAdapter _settingsAdapter;

        private IReadOnlyList<ArgMap> _argumentMappings;
        private IReadOnlyList<OptMap> _optionMappings;
        private CommandLineApplication _msCommand;
        private ICommandSettings _settings;
        private ICommand _command;

        public CommandSetup(ISettingsAdapter settingsAdapter)
        {
            _settingsAdapter = settingsAdapter;
        }

        #endregion


        public CommandSetup Initialize<TSettings>() where TSettings : ICommandSettings, new()
        {
            _settings = new TSettings();

            _argumentMappings = _settingsAdapter.TransformArgs<TSettings>();

            _optionMappings = _settingsAdapter.TransformOpts<TSettings>();

            return this;
        }

        public CommandSetup Configure<TCommand>(TCommand command, ref CommandLineApplication msApp) where TCommand : ICommand
        {
            _command = command;

            _msCommand = msApp.Command(_command.Name, c => { });

            _msCommand.Description = _command.Desc;

            _msCommand.ShowInHelpText = true;

            _msCommand.Arguments.AddRange(_argumentMappings.Select(a => a.MsArgument));

            _msCommand.Options.AddRange(_optionMappings.Select(o => o.MsOption));

            return this;
        }
        
        public CommandSetup Bind()
        {
            _msCommand.OnExecute(() =>
            {
                foreach (var arg in _argumentMappings)
                    arg.MapInputToSetting(ref _settings);

                foreach (var opt in _optionMappings)
                    opt.MapInputToSetting(ref _settings);

                return _command.Run(_settings);
            });

            return this;
        }
    }
}
