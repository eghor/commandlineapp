﻿using Microlite.Extensions.MsCommandLineUtils.Attributes;
using Microlite.Extensions.MsCommandLineUtils.Core.Adapter;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Internal
{
    internal class ArgumentsParser : Parser<ArgMap, ArgAttribute>
    {

        public override ArgMap ConfigureMapType(ArgAttribute attribute)
        {
            return new ArgMap
            {
                MsArgument = new CommandArgument
                {
                    Name = attribute.Name,
                    Description = attribute.Desc
                }
            };
        }
    }
}
