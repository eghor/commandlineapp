﻿using System;
using Microlite.Extensions.MsCommandLineUtils.Core;

namespace Microlite.Extensions.MsCommandLineUtils
{
    public abstract class Command<TSettings> : ICommand
    {

        public abstract string Name { get; }

        public abstract string Desc { get; }

        public abstract int Run(TSettings settings);

        int ICommand.Run(object settings)
        {
            return Run((TSettings) settings);
        }

        public Type SettingsType => typeof(TSettings);
    }
}
