﻿using System;
using System.Collections.Generic;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal interface IParser<TMap, in TAttr>
        where TMap : Map
        where TAttr : Attribute
    {
        TMap ConfigureMapType(TAttr attribute);

        List<TMap> Parse<TSettings>() where TSettings : ICommandSettings;
    }
}