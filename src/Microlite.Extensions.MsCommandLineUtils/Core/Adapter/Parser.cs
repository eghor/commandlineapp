﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal abstract class Parser<TMap, TAttr> : IParser<TMap, TAttr> where TMap : Map where TAttr : Attribute
    {
        public List<TMap> Parse<TSettings>() where TSettings : ICommandSettings
        {
            var mappings = new List<TMap>(50);

            var props = typeof(TSettings).GetTypeInfo().GetProperties(BindingFlags.Public | BindingFlags.Instance); //move to delegate

            mappings.AddRange(from prop in props
                let attr = prop.GetCustomAttribute<TAttr>() //move to delegate
                where attr != null
                select GetConfiguredMap(prop, attr));

            mappings.TrimExcess();

            return mappings;
        }

        protected TMap GetConfiguredMap(PropertyInfo prop, TAttr attribute)
        {
            var map = ConfigureMapType(attribute);

            map.SettingMember = prop;

            return map;
        }

        public abstract TMap ConfigureMapType(TAttr attribute);
    }
}
