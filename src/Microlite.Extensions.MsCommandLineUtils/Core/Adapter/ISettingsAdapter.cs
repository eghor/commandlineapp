﻿using System.Collections.Generic;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal interface ISettingsAdapter
    {
        List<ArgMap> TransformArgs<TSettings>() where TSettings : ICommandSettings;
        List<OptMap> TransformOpts<TSettings>() where TSettings : ICommandSettings;
    }
}