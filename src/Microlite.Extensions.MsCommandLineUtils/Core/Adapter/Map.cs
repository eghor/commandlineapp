﻿using System.Reflection;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal abstract class Map
    {
        public PropertyInfo SettingMember;

        public abstract void MapInputToSetting(ref ICommandSettings settings);
    }
}
