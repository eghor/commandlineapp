using System;
using System.ComponentModel;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Core.Adapter
{
    internal sealed class ArgMap : Map
    {
        public CommandArgument MsArgument;

        public override void MapInputToSetting(ref ICommandSettings settings)
        {
            MsArgument.ShowInHelpText = true;

            var converter = TypeDescriptor.GetConverter(SettingMember.PropertyType);
            var value = converter.ConvertFromInvariantString(MsArgument.Value);

            SettingMember.SetValue(settings, value);
        }
    }
}