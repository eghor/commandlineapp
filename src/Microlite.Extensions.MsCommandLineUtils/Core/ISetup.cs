﻿using Microlite.Extensions.MsCommandLineUtils.Internal;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils.Core
{
    interface ISetup
    {
        CommandSetup Bind();
        CommandSetup Configure<TCommand>(TCommand command, ref CommandLineApplication msApp) where TCommand : ICommand;
        CommandSetup Initialize<TSettings>() where TSettings : ICommandSettings, new();
    }
}