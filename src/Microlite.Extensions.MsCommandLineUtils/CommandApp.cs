﻿using System;
using Microlite.Extensions.MsCommandLineUtils.Core;
using Microlite.Extensions.MsCommandLineUtils.Internal;
using Microsoft.Extensions.CommandLineUtils;

namespace Microlite.Extensions.MsCommandLineUtils
{
    public sealed class CommandApp
    {

        #region init

        internal CommandLineApplication MsApp;
        internal ISetup Setup;

        public CommandApp()
        {
            MsApp = new CommandLineApplication();
            Setup = AppServices.GetSetup();
        }

        #endregion

        public void Register<TCommand,TSettings>() where TCommand : ICommand, new() where TSettings : ICommandSettings, new()
        {
            var command = new TCommand();

            Setup
                .Initialize<TSettings>()
                .Configure(command, ref MsApp)
                .Bind();
        }

        public void Execute(string[] args)
        {
            MsApp.HelpOption("-? | -h | --help");

            var res = MsApp.Execute(args);

            Environment.Exit(res);
        }
    }
}
